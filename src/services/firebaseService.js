import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const getParticipantDetails = async folio => {
  if (!folio) return null;

  const db = firebase.firestore();
  const newParticipantDoc = await db
    .collection("participants")
    .doc(folio)
    .get();
  const newParticipantData = newParticipantDoc.data();
  return newParticipantData;
};

const registerNewParticipant = async newParticipant => {
  const db = firebase.firestore();

  const isUnique = async (fieldName, compareTo) => {
    const collection = db.collection("participants");
    return (
      (await collection.where(fieldName, "==", compareTo).get()).docs.length ==
      0
    );
  };

  if (!(await isUnique("email", newParticipant.email))) {
    throw new Error("Email registrado anteriormente, pruebe con otro email.");
  }

  if (!(await isUnique("matricula", newParticipant.matricula))) {
    throw new Error(
      "Matrícula registrada anteriormente. No se puede registrar más de una vez al mismo estudiante."
    );
  }

  const newParticipantDoc = await db.collection("participants").add({
    email: newParticipant.email,
    nombre: newParticipant.nombre,
    matricula: newParticipant.matricula,
    celular: newParticipant.celular
  });
  return newParticipantDoc.id;
};

export default {
  getParticipantDetails,
  registerNewParticipant
};
