import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "jquery";
import "popper.js";
import "bootstrap";
import "./assets/app.scss";
import * as firebase from "firebase/app";

Vue.config.productionTip = false;

const firebaseConfig = {
  apiKey: "AIzaSyCKEju3Hug_jrWzTGGIp-aQcAfVy_2m_FI",
  authDomain: "fime-conf-7af58.firebaseapp.com",
  databaseURL: "https://fime-conf-7af58.firebaseio.com",
  projectId: "fime-conf-7af58",
  storageBucket: "fime-conf-7af58.appspot.com",
  messagingSenderId: "712946228170",
  appId: "1:712946228170:web:6440134b2dc28f76570d33"
};

firebase.initializeApp(firebaseConfig);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
