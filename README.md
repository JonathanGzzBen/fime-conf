# fime-conf

## Deployment

```shell
# Install firebase-tools
$ yarn global add firebase-tools

# If you are not logged in
$ firebase login

# Build and deploy
$ yarn build && firebase deploy
```

## Project setup

```shell
yarn install
```

### Compiles and hot-reloads for development

```shell
yarn serve
```

### Compiles and minifies for production

```shell
yarn build
```

### Lints and fixes files

```shell
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
